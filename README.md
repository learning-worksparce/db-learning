# db-learning

#### 介绍
数据库相关学习笔记（包括sql和nosql）

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1. 周期表（日表，月表，周表，年表，季表）中的日表和月表的自动扩展，已经接管了300多个周期表的自动管理

2. 从设计层面能够做到冷热数据分离和规避数据过度增长

3. **日表和月表什么关系呢？月表是日表的联合查询还是数据镜像？**

   日表和月表目前没有直接的关联，就是按照业务维度包括数据量进行综合评估选定的，如果有的业务数据量不大，范围查询多一些，就推荐月表，如果数据量抖动大，数据量大，而且还会有变更操作，一般建议是日表，我们日表和月表的比例差不多是20:1

4.  算前期规划，算是迭代改进，我们提供的一个福利就是改造成日表后，日表的扩展和数据清理都是我们来干了，业务很happy，而在以前，可能还会有手工维护Excel列表或者一些元数据配置的模式来记录不同业务的表的扩展情况，有种手工记账的感觉，如果DBA或者业务同学忘记了，基本碰上就是一次数据故障。

5.  写了自动管理的服务，包括单机和集群中间件的周期表管理，基本上我们就不用手工干预了。

6.  对于业务来说很大的痛点就是表如何扩展（有时候忘记了后果挺严重的），数据清理（如果不拆表，按照delete模式很痛苦）和表变更（T+1的模式对于业务来说是可用接受的，对于DBA完全可控）

7.  

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

# MySQL周期表管理的设计

https://blog.csdn.net/yangjianrong1985/article/details/102479302

https://blog.csdn.net/weixin_39801165/article/details/113724648

https://blog.csdn.net/weixin_35098081/article/details/113641426
