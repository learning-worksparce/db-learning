## 字符串函数与操作符

| 函数                                                         | 返回值类型   | 描述                                                         | 示例                                                         | 结果                             |
| ------------------------------------------------------------ | ------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | -------------------------------- |
| string\|\|string                                             | text         | 字符串连接                                                   | select 'Post'\|\|'gresql'\|\|' good!';                       | Postgresql good!                 |
| string\|\|non-string或non-string\|\|string                   | text         | 非字符串与字符串连接                                         | select 1\|\|' one';                                          | 1 one                            |
| bit_length(string)                                           | int          | 字符串的位数，注意一个汉字是3个字节                          | select bit_length('one');                                    | 24                               |
| char_length(string)                                          | int          | 字符串中字符的个数                                           | select char_length('中国');                                  | 2                                |
| length(string)                                               | int          | 字符串中字符个数，同char_length(string)                      | select length('中国');                                       | 2                                |
| length(string,encoding name)                                 | int          | 以name的编码格式字符串的字符个数                             | select length('中国','GBK');                                 | 3                                |
| octet_length(string)                                         | int          | 字符串的字节数                                               | select octet_length('中国');                                 | 6                                |
| lower(`string`)                                              | text         | 将字符串转换成小写                                           | select lower('HeLLO');                                       | hello                            |
| upper(string)                                                | text         | 将字符串转换成大写                                           | select lower('HellO');                                       | HELLO                            |
| initcap(string)                                              | text         | 将字符串中每个单词的首字母大写                               | select initcap('hello world !');                             | Hello World !                    |
| overlay(`string` placing `string` from `int` [for `int`])    | text         | 替换字符串，其中第一个int是开始位置，第二个int是长度，如果没有第二个int则长度默认为第二个字符串的长度 | select overlay('Txxxxas' placing 'hom' from 2 for 4);        | Thomas                           |
| replace(string,string,string)                                | text         | 替换字符串，将第一个字符串中的第二个字符串替换成第三个字符串 | select replace('Txxxxas','xxxx','hom');                      | Thomas                           |
| translate(`string` `text`, `from ``text`, `to` `text`)       | text         | 把string中from字符替换成对应to中的字符，如('text','tx','nd')中t->n,x->d,替换后结果为nedn;如果from比to长，则删除from多出的字符，如('Txxxxas','Txas','hom')结果为hooom;如果from中有重复字符，对应顺序也是一一对应的，但是重复字符以第一个为准，如('txxxa','xxx','abcd'),结果为taaad | select translate('Txxxxas','xxxxa','hom');select translate('Txxxxas','Txas','hom');select translate('txxxa','xxxa','abcd'); | Thhhhshoooomtaaad                |
| position(`substring` in `string`)                            | int          | 给定子字符串在字符串的位置                                   | select position('lo' in 'hello');                            | 4                                |
| strpos(`string`, `substring`)                                | int          | 功能同上，只是入参顺序相反                                   | select strpos('hello','lo');                                 | 4                                |
| substring(`string` from `int ` [for `int`])                  | text         | 截取字符串，从from位置截取长度for，如果for省略，则是从from至结尾 | select substring('hello world' from 2 for 3);select substring('hello world' from 2); | ellell world                     |
| substring(string,from int[, for int])                        | text         | 同上                                                         | select substring('hello world',2,3);                         | ell                              |
| substring(`string` from `pattern`)                           | text         | 截取匹配posix正则表达式的字符串                              | select substring('hello world' from '...$');                 | rld                              |
| substring(`string` from `pattern` for `escape`)              | text         | 截取匹配posix正则表达式的字符串,for为转移字符                | select substring('Thomas' from '%#"o_a#"_' for '#');         | oma                              |
| trim([leading \| trailing \| both][`characters`] from `string`) | text         | 删除字符串头部（leading）,尾部(trailing)或两边的空白活[characters]字符 | select trim(both 'x' from 'xxjdhdxxxx');                     | jdhd                             |
| trim([leading \| trailing \| both][from] `string`[`, characters`] ) | text         | 同上                                                         | select trim(both from '  jdhd  ',' ');                       | jdhd                             |
| btrim(`string` `text` [, `characters ``text`])               | text         | 删除字符串两边指定的字符                                     | select btrim('xxhhhxxx','x');                                | hhh                              |
| rtrim(`string` `text` [, `characters``text`])                | text         | 删除字符串尾部指定的字符                                     | select rtrim('xxhhhxxx','x');                                | xxhhh                            |
| ltrim(`string` `text` [, `characters``text`])                | text         | 删除字符串开头指定的字符                                     | select ltrim('xxhhhxxx','x');                                | hhhxxx                           |
| ascii(`string`)                                              | int          | 字符串第一个字符的ASCII值                                    | select ascii('xa');select ascii('x');                        | 120120                           |
| chr(`int`)                                                   | text         | 将数字转换成字符                                             | select chr(65);                                              | A                                |
| concat(`str` `"any"` [, `str` `"any"` [, ...] ])             | text         | 连接所有参数，个数不限，类型不限                             | select concat('x','man',3);                                  | xman3                            |
| concat_ws(`sep` `text`, `str` `"any"` [,`str` `"any"` [, ...] ]) | text         | 功能同上，只是第一个参数是连接分隔符                         | select concat_ws(',','x','man',3);                           | x,man,3                          |
| convert(`string` `bytea`,`src_encoding` `name`, `dest_encoding``name`) | text         | 将字符串从指定编码转换至目的编码格式                         | select convert('Hello','UTF8','GBK');                        | \x48656c6c6f                     |
| `format`(`formatstr` `text` [,`formatarg` `"any"` [, ...] ]) | text         | 格式化字符串，类似C语言的sprintf,其中n$表示第n个参数         | select format('Hello %s, %1$s', 'World');                    | Hello World, World               |
| left(`str` `text`, `n` `int`)                                | text         | 返回字符串前n个字符，n为负数时返回除最后\|n\|个字符以外的所有字符 | select left('hello',-2);                                     | hel                              |
| right(`str` `text`, `n` `int`)                               | text         | 返回字符串后n个字符，n为负数时返回除最前\|n\|个字符意外的所有字符 | select right('hello',2);                                     | he                               |
| lpad(`string` `text`, `length` `int` [,`fill` `text`])       | text         | 在字符串开头填充text至长度为length，缺省为空白，如果string的长度已经大于length，则会截断后面多余length的字符 | select lpad('123',5,'0');                                    | 00123                            |
| rpad(`string` `text`, `length` `int` [,`fill` `text`])       | text         | 在字符串尾部填充text至长度为length，缺省为空白，如果string的长度已经大于length，则会截断后面多余length的字符 | select rpad('he',1,'o');                                     | h                                |
| md5(`string`)                                                | text         | 计算string的md5散列值，并以十六进制返回                      | select md5('hello');                                         | 5d41402abc4b2a76b9719d911017c592 |
| parse_ident(`qualified_identifier``text` [, `strictmode` `boolean`DEFAULT true ] ) | text[]       | 将qualified_identifier拆分解析到一个数组中，以句点为分隔符。 | select parse_ident('SomeSchema.someTable');                  | {someschema,sometable}           |
| pg_client_encoding()                                         | name         | 获取当前客户端编码                                           | select pg_client_encoding();                                 | UTF8                             |
| quote_ident(`string` `text`)                                 | text         | 返回适合SQL语句标志符且使用适当引号的字符串，在字符串两端加双引号，如果字符串中出现双引号，返回结果中将变成两个，如果有2个连续的单引号，返回时只有1个 | select quote_ident('Foo"''"bar');                            | "Foo""'""bar"                    |
| quote_literal(`string` `text`)                               | text         | 功能同上，只是内嵌的单引号和双引号被原样保留                 | select quote_literal('Foo"''bar');                           | 'Foo"''bar'                      |
| quote_literal(`value` `anyelement`)                          | text         | 将给定值转成text                                             | select quote_literal(45);                                    | '45'                             |
| quote_nullable(`string` `text`)                              | text         | 功能同quote_literal(`string` `text`)，只是参数是NULL时返回NULL |                                                              |                                  |
| quote_nullable(`value` `anyelement`)                         | text         | 功能同quote_literal(`value` `anyelement`)，只是参数为NULL时返回NULL |                                                              |                                  |
| repeat(`string` `text`, `number` `int`)                      | text         | 将string重复number次                                         | select repeat('Hi',2);                                       | HiHi                             |
| split_part(`string` `text`,`delimiter` `text`, `field` `int`) | text         | 将字符串string以delimiter进行分割，并返回第field个子串       | select split_part('1#2#3','#',2);                            | 2                                |
| to_hex(`number` `int` or `bigint`)                           | text         | 将数值转换成十六进制                                         | select to_hex(155);                                          | 9b                               |
| reverse(`str`)                                               | text         | 将字符串逆序输出                                             | select reverse('hello');                                     | olleh                            |
| regexp_split_to_array(`string``text`, `pattern` `text` [, `flags` `text`]) | text[]       | 将字符串匹配posix正则表达式分割为字符串数组                  | select regexp_split_to_array('hello world', E'\\s+');        | {hello,world}                    |
| regexp_split_to_table(`string``text`, `pattern` `text` [, `flags``text`]) | setoftext    | 功能同上，只是以单列形式返回                                 | select regexp_split_to_table('hello world', E'\\s+');        | hello world                      |
| regexp_matches(`string` `text`,`pattern` `text` [, `flags` `text`]) | setof text[] | 返回string中第一个匹配posix正则表达式的子串，如果flag=g，则返回所有 | select regexp_matches('foobarbequebaz', '(b..)','g');        | {bar} {beq} {baz}                |
| regexp_replace(`string` `text`,`pattern` `text`, `replacement` `text`[, `flags` `text`]) | text         | 将匹配posix正则表达式的第一个子串替换成指定字符串，如果flag=g，则替换所有 | select regexp_replace('Thomas', '.[mN]a.', 'M');             | ThM                              |

format格式化

格式说明符由 % 字符引进，格式为

%[ position ] type
组件的字段有：
position (optional)
n$ 格式的字符串，这里的n是要打印的参数的索引。索引为1表示在formatstr之后的第一个参数。如果省略了position，默认使用序列中的下一个参数。
type (required)
格式转换的类型用来产生格式说明符的输出。支持下列的类型：
s 格式参数值为简单的字符串。空值作为空字符串对待。
I 将参数值作为SQL标识符对待，如果需要，双写它。值为空是错误的。
L 引用参数值作为SQL文字。空值用字符串 NULL 显示，没有引用。
除了上述的格式说明符，特殊的序列 %% 可以用作输出 % 字符。



示例：

```
test=# SELECT format('Hello %s', 'World');
   format    
-------------
 Hello World
(1 row)

test=# SELECT format('Testing %s, %s, %s, %%', 'one', 'two', 'three');
           format           
----------------------------
 Testing one, two, three, %
(1 row)

test=# SELECT format('INSERT INTO %I VALUES(%L)', 'Foo bar', E'O\'Reilly');
                  format                   
-------------------------------------------
 INSERT INTO "Foo bar" VALUES('O''Reilly')
(1 row)
```

 



## 锁

``` sql
select * from pg_locks
where locktype ='relation' and relation = 25243;
```

AccessShareLock 只是共享读锁，只有需要AccessExclusiveLock锁的语句比如alter table 等才会等待该锁会被释放。

 ExclusiveLock 这个锁一般是全局资源的排它锁，我猜测是xid 对应的排它锁，保证在该事务之内的查询的锁机制

``` sql
select * from pg_locks
where relation in (
	select oid from pg_class where relname='bmc_r03_by_catagory_report'
);
```

``` sql
SELECT * FROM pg_stat_activity 
where pid = '1422'
and state = 'active' 
and client_addr = '10.122.25.197';

SELECT pg_cancel_backend(pid);

SELECT pg_cancel_backend(1864);


select  pg_terminate_backend(17262);

--看pgsql的表是否锁住
select oid from pg_class where relname='表名'
select pid from pg_locks where relation='上面检索出的oid'

--如果能检索出结果，则使用下面的语句逐个kill掉进程(我的理解)，进程编号是刚才查的pid
select pg_terminate_backend('这里是上面的检索结果')
--或者
select pg_cancel_backend('这里是上面的检索结果')

select * from pg_locks
where relation in (
	select oid from pg_class where relname='bmc_r03_by_catagory_report'
);


SELECT rd.service_mtm_des, rd.service_mtm_ph FROM bmc_r03_by_catagory_report rd 
where rd.service_mtm = '5WS0Q11450' and rd.id <> '2021-HK01-0829958328-065-A--000000-2021003-GLPCA' and rd.service_mtm_des is not null limit 1


```

