路径：C:\\Users\\imche\\AppData\\Roaming\\DBeaverData\\workspace6

- credentials-config.json
- data-sources.json
- project-metadata.json



读取配置文件中的配置：

``` java
import com.alibaba.fastjson.JSONObject;
import com.lsump.ops.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;

/**
 * 配置读取 测试
 *
 * @author chentiefeng
 * @date 2021/7/5 18:12
 */
@Slf4j
public class ConfigReadTest {
    private static final byte[] LOCAL_KEY_CACHE = new byte[] { -70, -69, 74, -97, 119, 74, -72, 83, -55, 108, 45, 101, 61, -2, 84, 74 };
    public static final int DEFAULT_BUFFER_SIZE = 16384;
    public static void main(String[] args) throws IOException {
        String path = "C:\\Users\\imche\\AppData\\Roaming\\DBeaverData\\workspace6\\lsum\\.dbeaver\\";
        File credFile = new File(path, "credentials-config.json");
        String credJson  = loadConfigFile(credFile, true);

        File dataSource = new File(path, "data-sources.json");
        String dataSourceJson  = loadConfigFile(dataSource, false);

        JSONObject credJsonObj = JsonUtils.toObject(credJson);
        JSONObject jsonObject = JsonUtils.toObject(dataSourceJson);
        JSONObject connectionsJson = jsonObject.getJSONObject("connections");
        for (String tmpKey: connectionsJson.keySet()) {
            JSONObject configurationJson = connectionsJson.getJSONObject(tmpKey).getJSONObject("configuration");
            System.out.println("environment:" + configurationJson.getString("type") + "_" + configurationJson.getString("database"));
            System.out.println("Url:" + configurationJson.getString("url"));
            JSONObject passJson = credJsonObj.getJSONObject(tmpKey);
            JSONObject connectionJson = passJson.getJSONObject("#connection");
            System.out.println("username:" + connectionJson.getString("user"));
            System.out.println("password:" + connectionJson.getString("password"));
            System.out.println("_______________________________________________");
        }
        System.out.println();
    }

    private static String loadConfigFile(File credFile, boolean decrypt) throws IOException {
        ByteArrayOutputStream credBuffer = new ByteArrayOutputStream();
        try (InputStream crdStream = new FileInputStream(credFile)) {
            copyStream(crdStream, credBuffer);
        } catch (Exception e) {
            log.error("Error reading secure credentials file", e);
        }
        if (!decrypt) {
            return new String(credBuffer.toByteArray(), StandardCharsets.UTF_8);
        } else {
            SecretKey secretKey = new SecretKeySpec(LOCAL_KEY_CACHE, "AES");
            try {
                return decrypt(secretKey, credBuffer.toByteArray());
            } catch (Exception e) {
                throw new IOException("Error decrypting encrypted file", e);
            }
        }
    }

    public static String decrypt(SecretKey secretKey, byte[] contents) throws InvalidAlgorithmParameterException, InvalidKeyException, IOException {
        Cipher cipher;
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        } catch (Exception e) {
            throw new IllegalStateException("Internal error during encrypted init", e);
        }
        try (InputStream byteStream = new ByteArrayInputStream(contents)) {
            byte[] fileIv = new byte[16];
            byteStream.read(fileIv);
            cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(fileIv));

            try (CipherInputStream cipherIn = new CipherInputStream(byteStream, cipher)) {
                ByteArrayOutputStream resultBuffer = new ByteArrayOutputStream();
                copyStream(cipherIn, resultBuffer);
                return new String(resultBuffer.toByteArray(), StandardCharsets.UTF_8);
            }

        }
    }

    public static void copyStream(
            InputStream inputStream,
            OutputStream outputStream)
            throws IOException {
        copyStream(inputStream, outputStream, DEFAULT_BUFFER_SIZE);
    }

    /**
     * Read entire input stream and writes all data to output stream
     * then closes input and flushed output
     */
    public static void copyStream(
            InputStream inputStream,
            OutputStream outputStream,
            int bufferSize)
            throws IOException {
        try {
            byte[] writeBuffer = new byte[bufferSize];
            for (int br = inputStream.read(writeBuffer); br != -1; br = inputStream.read(writeBuffer)) {
                outputStream.write(writeBuffer, 0, br);
            }
            outputStream.flush();
        } finally {
            // Close input stream
            inputStream.close();
        }
    }
}

```

