# 安装

## Docker安装Mysql

1、拉取镜像

```
docker pull mysql:5.7
```

2、启动镜像

```
docker run -p 3306:3306 --name mysql \
-v /workspace/mysql/log:/var/log/mysql \
-v /workspace/mysql/data:/var/lib/mysql \
-v /workspace/mysql/conf:/etc/mysql \
-e MYSQL_ROOT_PASSWORD=123456 \
-d mysql:5.7
```

参数说明：

- `-p 3306:3306` 将容器的3306端口映射到主机的3306端口
  - `-v /workspace/mysql/log:/var/log/mysql`将日志文件夹挂载到主机
  - `-v /workspace/mysql/data:/var/lib/mysql`将数据文件夹挂载到主机
  - `-v /workspace/mysql/conf:/etc/mysql` 将配置文件夹挂载到主机
  - `-e MYSQL_ROOT_PASSWORD=123456`初始化Mysql中root用户的密码

3、启动之后，就可以连接了。

4、修改mysql的配置文件

创建配置文件

``` shell
vi /workspace/mysql/conf/my.cnf
```

内容如下：

``` properties
[client]
default-character-set=utf8

[mysql]
default-character-set=utf8

[mysqld]
init_connect='set collation_connection = utf8_unicode_ci'
init_connect='SET NAMES utf8'
character-set-server=utf8
collation-server=utf8_unicode_ci
skip-character-set-client-handshake
skip-name-resolve
```

5、重启mysql

``` shell
docker restart mysql
```

6、自动重启

```shell
docker update mysql --restart=alway
```

## 





## MySQL中的各种锁

**行级锁：** 行级锁是 MySQL 中锁定粒度最细的一种锁，表示只针对当前操作的行进行加锁。行级锁能大大减少数据库操作的冲突，其加锁粒度最小，但加锁的开销也最大。行级锁分为共享锁和排他锁。开销大，加锁慢；会出现死锁；锁定粒度最小，发生锁冲突的概率最低，并发度也最高。

**表级锁：** 表级锁是 MySQL 中锁定粒度最大的一种锁，表示对当前操作的整张表加锁，它实现简单，资源消耗较少，被大部分 MySQL 引擎支持。最常使用的 MyISAM 与 InnoDB 都支持表级锁定。表级锁定分为表共享读锁（共享锁）与表独占写锁（排他锁）。开销小，加锁快；不会出现死锁；锁定粒度大，发出锁冲突的概率最高，并发度最低。

**页级锁：**　页级锁是 MySQL 中锁定粒度介于行级锁和表级锁中间的一种锁。表级锁速度快，但冲突多，行级冲突少，但速度慢。因此，采取了折衷的页级锁，一次锁定相邻的一组记录。BDB 支持页级锁。开销和加锁时间界于表锁和行锁之间；会出现死锁；锁定粒度界于表锁和行锁之间，并发度一般。

### 出现问题

**丢失修改：** 指事务1和事务2同时读入相同的数据并进行修改，事务2提交的结果破坏了事务1提交的结果，导致事务1进行的修改丢失。

**不可重复读：** 一个事务在读取某些数据后的某个时间，再次读取以前读过的数据，却发现其读出的数据已经发生了改变、或某些记录已经被删除了！

**读脏数据：** 事务T1修改某一数据，并将其写回磁盘，事务T2读取同一数据后，T1由于某种原因被撤消，这时T1已修改过的数据恢复原值，T2读到的数据就与数据库中的数据不一致，则T2读到的数据就为"脏"数据，即不正确的数据。

**死锁：** 两个或两个以上的进程在执行过程中，由于竞争资源或者由于彼此通信而造成的一种阻塞的现象，若无外力作用，它们都将无法推进下去。此时称系统处于死锁状态或系统产生了死锁，这些永远在互相等待的进程称为死锁进程





















